package dev.qlab.mc.helloworldmod.core.init;

import dev.qlab.mc.helloworldmod.HelloWorldMod;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public final class HelloWorldModItems {

    static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, HelloWorldMod.MODID);

    public static void init() {
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<Item> RESISTOR_0402_ITEM = ITEMS.register("resistor_0402_item", () ->
        new Item(
            new Item.Properties().tab(HelloWorldMod.HELLOWORLDMOD_TAB)
            )
    );

    // block items
    public static final RegistryObject<BlockItem> ZINC_BLOCKITEM = ITEMS.register("zinc_ore", () ->
        new BlockItem(HelloWorldModBlocks.ZINC_ORE.get(),
        new Item.Properties().tab(HelloWorldMod.HELLOWORLDMOD_TAB)));
}
