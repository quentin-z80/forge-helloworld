package dev.qlab.mc.helloworldmod;

import dev.qlab.mc.helloworldmod.core.init.HelloWorldModBlocks;
import dev.qlab.mc.helloworldmod.core.init.HelloWorldModItems;
import dev.qlab.mc.helloworldmod.utils.CustomCreativeTab;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraftforge.fml.common.Mod;

@Mod(HelloWorldMod.MODID)
public class HelloWorldMod {

    public static final String MODID = "helloworldmod";

    public HelloWorldMod() {

        //IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        HelloWorldModBlocks.init();
        HelloWorldModItems.init();

    }

    public static final CreativeModeTab HELLOWORLDMOD_TAB = new CustomCreativeTab(MODID, HelloWorldModItems.RESISTOR_0402_ITEM);

}
